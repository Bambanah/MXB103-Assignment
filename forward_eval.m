function y = forward_eval(X, FDT, x)
%FORWARD_EVAL Evaluate Newton's forward difference form of the
% interpolating polynomial
% y = FORWARD_EVAL(X, FDT, x) returns y = Pn(x), where Pn is the
% interpolating polynomial constructed using the abscissas X and
% forward difference table FDT

[m,n] = size(FDT);
if m ~= n
    error('FDT must be square.');
end

h = X(2)-X(1);        % the equally spaced sub-interval length
s = (x - X(1))/h;     % polynomial in terms of s

y = zeros(size(x));    % initalise sum
for k = 1: n
    P = ones(size(x)); % initalise Product
    for j = 1:k-1
        P = P.*(s-j+1)/j; %loop over the product
    end
    y = y + FDT(k,k).*P ;  % add the next term
end
