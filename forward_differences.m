function T = forward_differences(Y) 
% T = forward_difference(Y) returns Newton's forward difference table.
% The forward difference table is laid out in the matrix T as:
%y0
%y1   del y0
%y1   del y1   del^2 y0
%y2   del y2   del^2 y2 del^3 y0
% etc.
%The rest of matrix T is zero.


%Construct the empty forward difference table
n = length(Y);
T = zeros(n,n);

% Fill the first column
T(:,1) = Y;

%Fill the remaining columns
for j = 2:n
    for i = j:n
        T(i,j) = T(i,j-1) - T(i-1, j-1);
    end
end

