%% MXB103 Project Group 53:  BUNGEE!
% Lachlan Underhill, Jordan Cosgrove, Aramis-Ali Ahdizadeh-Rios

%%
close all; clc; clear variables;

%% 1  Introduction
% As part of Brisbane's "New World City" transformation, the Brisbane City
% Council is investigating a proposal to allow bungee jumping off the
% Story Bridge. This report addresses several key questions about the
% proposal.

%% 2  The Proposal
%
% The proposal calls for a platform to be installed at the top of Story
% Bridge to enable a bungee jumping experience for clients. A camera is to
% be installed on a lower bridge deck and  be set to trigger at a
% predetermined time to photograph the jumper as they first pass this
% point.
%
% We set out to answer several questions:
% 
%  1. Can a standard jump of 10 "bounces" in 60 seconds be achieved?
%
%  2. What is the maximum speed of the jumper and where does it occur in
%   relation to the overall jump?
%
%  3. What is the maximum acceleration of the jumper and when does it
%   occur in relation to the overall jump? Can a claim of "up to 2g"
%   acceleration be met?
%   
%  4. What is the total distance the jumper travels in the 60 second jump?
%
%  5. For the camera installed on the deck at height D, when should the
%   camera be triggered to photograph the jumper?
%
%  6. How close does the jumper come to touching the water for the given
%   parameters? What modifications to the bungee rope can be instituted to
%   allow a true water touch experience while maintaining a 10 bounce jump
%   in 60 seconds and not exceeding an acceleration of 2g for safety
%   reasons.
%
% Each of these questions will be answered through developing a
% mathematical model to simulate the bungee jumping process. Successfully 
% answering these questions with positive results will give
% credence to further pursue this proposal and its inclusion within the
% 'New World City' transformation. 
% 
% Various appropriate numerical methods will be applied at each step and 
% implemented using MATLAB.
%
% In order to create a suitable simulation, an equation for the motion of 
% a bungee jumper must first be constructed that uses the known parameters
% of the proposed system.

%% 3 The Model
%
% The equation of motion for bungee jumping is
%
% $$\frac{dv}{dt} = g - C|v|v - \max(0,K(y-L))$$
%
% The model uses Newton�s second law of motion: The sum of the forces must
% equal the product of the jumper�s mass and acceleration. The forces
% acting on the jumper are:
% 
% * Gravity $$mg$$ 
%
% * Drag $$-c|v|v$$
%
% * Tension $$-max(0, k(y-L))$$
%
% The model is that of a mass-spring system with additional damping or drag
% force that is proportional to the velocity squared. The drag is due to
% the friction and movement through the fluid (air) and acts in a direction
% opposite to the motion. The downward direction is chosen as positive. The
% gravitational force is always acting downwards, the drag force always
% opposes instantaneous motion and  the bungee rope is like that of a
% spring and while under tension, by Hook'es law is a restoring force.
%
% The equation governing the bungee jumping is the following ordinary
% differential equation (ODE):
%
% $$m dv/dt = mg-c|v|v-max(0,k(y-L))$$
%
% where dv/dt is the jumper�s acceleration.  The equation can be simplified
% by dividing through by m, to obtain
%
% $$dv/dt = g-C|v|v-max(0,K(y-L))$$
%
% where:
% 
%   v = the velocity of the jumper 
%
%   dv/dt= the acceleration of the jumper 
%
%   t = time g = gravitational acceleration  9.8 m.(sec)^-2 
%
%   y = the distance travelled by the bungee jumper
%
%   L = the length of the unstretched rope
%
%   H = the height of the platform from the water level
%
%   c = the drag coefficient (kg/m) 
%
%   C = c/m, is the scaled drag coefficient
%
%   k = the spring constant, which measures the elasticity of the bungee
%   rope
%
%   K = k/m  is the scaled spring constant

%% 3.1 Assumptions and Limitations
%
% The bungee jump  model is based on the jumper having a weight of 80kg and
% does not look at how variations in this value will affect the behaviour
% of the jump. Variations in weather or tidal differences in the Brisbane
% river are not inputs to this model and may affect the maximum
% acceleration and 'water touch' aspects of the jump. There is no data
% supplied on whether everyone uses the same type of harness or the type of
% clothing that must be worn. These could impact on drag resistance. The
% parameters provide only one value for the spring constant of the bunjee
% rope, rather than a range of values. Only limited investigation into
% various rope lengths and spring constants are looked at in this report.

%% 3.2 Parameters

H = 74;             % Height of jump point (m)
D = 31;             % Deck height (m)
c = 0.9;            % Drag coefficient (kg/m)
m = 80;             % Mass of the jumper (kg)
L = 25;             % Length of bungee cord (m)
k = 90;             % Spring constant of bungee cord (N/m)
g = 9.8;            % Gravitational acceleration (m/s^2)
C = c/m;            % Scaled drag coefficient
K = k/m;            % Scaled spring constant

%% 4 The Numerical Method
%
% How do you formulate the model to solve numerically? (hint: you write it
% as two equations)
%
% For numerical purposes we can model this problem as two ODEs:
%
% * Velocity
% $$dy/dt = v$$
%
% where velocity v is the derivative of the bungee jumper's position
%
% $$dv/dt = g-C|v|v-max(0,K(y-L))$$
%
% where the acceleration is the derivative of the bungee jumper's velocity

%% 4.1 Parameters

T = 60;             % Final time in simulation (s)
n = 10000;          % Number of subintervals

%% 4.2 Solution
%
% The ordinary differential equations are solved using fourth order
% Runge-Kutta method.
[t, y, v, h] = rk4_bungee(T, n, g, C, K, L);

% Plot position graph
figure(1);
plot(t, y);
xlabel('time (s)');
ylabel('distance fallen (m)');
title('Figure 1: Position of Jumper Over Time');

%% 5 Analysis
%
%% 5.1 Timing and bounces
%
% Does the jumper achieve at least 10 bounces in 60 seconds?
%
% This can be found by finding the number of peaks in the height data. Each
% peak represents one bounce.
% 

% Section title
fprintf("1. How many bounces occur in 60 Seconds?\n");

% Find peaks
num_peaks = numel(findpeaks(y));

% Display result
fprintf("Number of Bounces: %d\n", num_peaks);

% Confirm # of bounces >= 10
fprintf("Does the jumper achieve 10 bounces in 60 seconds?\n")
if num_peaks >= 10
    fprintf("Yes.\n")
else
    fprintf("No.\n")
end

%% 5.2 Maximum speed experienced by the jumper
%
% What is the maximum speed of the jumper and where does it occur in
% relation to the overall jump?
%
% Maximum velocity of the jumper can be found by finding the maximum value
% in the velocity vector produced by the RK4 function in section 4.2. The
% index of that maximum value is used to find the time at which this
% velocity is achieved.
% 

% Section title
fprintf("\n2. Thrill Factor\n");

% Plot velocity graph
figure(2);
plot(t, v);
xlabel('time (s)');
ylabel('velocity (m/s)');
title('Figure 2: Velocity of Jumper Over Time');

% Get max. velocity
[max_v, max_v_index] = max(v);
max_v_time = t(max_v_index);

% Display result
fprintf("Maximum Velocity: %.2fm/s at %.1fs\n", max_v, max_v_time);


%% 5.3 Maximum acceleration experienced by the jumper
%
% What is the maximum acceleration of the jumper and when does it
% occur in relation to the overall jump? Can a claim of "up to 2g"
% acceleration be met?
%
% To find acceleration, the vector for velocity must be numerically
% differentiated. After this, the maximum can be found in the same way as
% velocity in the previous question.
% 
% It is important to note that acceleration can be both positive and
% negative, so the highest values in each direction must be compared.

% Section title
fprintf("\n3. Acceleration\n");

% Derive acceleration
a = derive(t, v);

% Plot acceleration graph
figure(3);
plot(t, a);
xlabel('time (s)');
ylabel('acceleration (m/s^2)');
title('Figure 3: Acceleration of Jumper Over Time');

% Find max. acceleration (pos or neg)
if abs(max(a)) > abs(min(a))
    [max_a, max_a_index] = max(a);
else
    [max_a, max_a_index] = min(a);
end
max_g = max_a/9.8;
max_a_time = t(max_a_index);

% Display result
fprintf("Maximum acceleration: %.2fm/s^2 (%.2fg) at %.2fs\n", max_a, max_g, max_a_time);

% Display Test
if (max_g <= 2 && max_g >= -2)
    fprintf("Acceleration is within advertised bounds of 2g\n");
else
    fprintf("Acceleration is greater than advertised bounds of 2g\n");
end

%% 5.4 Total Distance Travelled
%
% What is the total distance the jumper travels in the 60 second jump?
% 
% Simpson's Rule is used to numerically integrate $$|v|$ to find the total
% distance covered.

fprintf("\n4. Total Distance\n");

% Get total distance travelled
total_s = simpsonsrule( abs(v), 0, 60, n);

% Display result
fprintf('Total Distance Travelled: %.2fm\n', total_s);

%% 5.5 Automated Camera System
%
% For the camera installed on the deck at height D, when should the
%    camera be triggered to photograph the jumper?
%   
% TODO: Rewrite below
% In this case, you will
% fit an interpolating polynomial through the four points in your solution
% $$y$ that lie either side of the camera location.  Then use that
% polynomial to solve for when the jumper passes the camera.

fprintf('\n5. Camera Timing\n');

% 5a. Find values of yi such that yi, yi+1 < H-D and yi+2, yi+3 > H-D.

% Initialise counter
 i = 1;
 
% Find the point at which y passes H-D
 while y(i+1) < H-D
     i = i+1 ;
 end

 % Save four surrounding points of y and t
 y_camera = y(i-1:i+2);
 t_camera = t(i-1:i+2);  
 
%  fprintf("H-D = %.2f, y_camera = %.2f, %.2f, %.2f, %.2f", H-D, y_camera);

% 5b. Find the interpolating polynomial through the points of
% (t_camera, Y_camera) using the function forward_differences(Y_camera)
% for equally spaced abscissa. Function forward_eval then returns the set
% of data points for the polynomial.

forward = forward_differences(y_camera);
t_points = t_camera(1): 0.001:t_camera(end);
pn = forward_eval(t_camera, forward,t_points); 

% 5c. To determine the value of t at which time the camera should be
% triggered, requires solving p(t) = H - D. 
% Numerically this is accomplished by employing a root finding method to
% establish where p(t) = pn -(H-D) = 0. The method used is the secant
% method.

tdata = t(i-2:i+1);
ydata = y(i-2:i+1);
s= @(t) ( t - tdata(1))/h; 
pt = @(t)42.8718 + 0.0902*s(t) - 0.0005*s(t) * (s(t)-1)-H+D;
trigger_time = secant(pt,3.3240, 3.3420, 0.1, 200);

% 5d. Hence, for the model paramaters provided, at what time should the
% camera trigger?

fprintf('In order to capture the image of the jumper the camera should trigger at t = %4.3f seconds\n\n', trigger_time)

%% 5.6 Water Touch Option
%
% How close does the jumper come to touching the water for the given
% parameters? What modifications to the bungee rope can be instituted to
% allow a true water touch experience while maintaining a 10 bounce jump
% in 60 seconds and not exceeding an acceleration of 2g for safety
% reasons.
%
% A range of length (L) and tension (k) values were tested to determine the
% combination that produces the closest jump to the water surface.

fprintf('\n6. Water Touch\n');

% Default values:
% L = 25m
% k = 90 N/m
min_y = H - max(y);
fprintf("Default values of L and k come within %.2fm of the water.\n", min_y);

% Define variables
vector_n = 50;       % Number of subintervals

range_L_plus = 50;   % Upper limit of L values
range_L_minus = 20;   % Lower limit of L values

range_k_plus = 80;   % Upper limit of k values
range_k_minus = 50;  % Lower limit of k values

min_bounces = 9;       % Minimum number of bounces
min_y_threshold = 0; % Minimum height at bottom of fall

% Create vectors for L and K values
L_vector = linspace(range_L_minus, range_L_plus, vector_n);
K_vector = linspace(range_k_minus/m, range_k_plus/m, vector_n);

fprintf("\nComputing %d possible combinations of L and k...\n", ...
    length(L_vector)*length(K_vector));

distance_values = NaN(length(L_vector) * length(K_vector), 6);

for i=1:length(L_vector)    
    for j=1:length(K_vector)
        % Compute results of changing L and k
    
        % Calculate using rk4 method
        [t, y, v, ~] = rk4_bungee(T, n, g, C, K_vector(j), L_vector(i));

        % Derive acceleration
        a = derive(t, v);

        % Find max. acceleration (pos or neg)
        if abs(max(a)) > abs(min(a))
            [max_a, max_a_index] = max(a);
        else
            [max_a, max_a_index] = min(a);
        end
        max_g = max_a/g;

        % Get maximum y value (bottom of fall)
        [max_y, max_y_index] = max(y);

        % Get number of bounces
        num_bounces = numel(findpeaks(y));

        % Only save data if acceleration < 2g and jumper doesn't go into the
        % water
        if (abs(max_g) <= 2 && max_y <= (H - min_y_threshold) && num_bounces >= min_bounces)
            distance = H - max_y;

            distance_values((i-1)*25+j,:) = [t(max_y_index) distance num_bounces max_g L_vector(i) K_vector(j)*m];
        end
        
    end
end

% Get closest distance value
[~, closest_index] = min(distance_values(:,2));
closest_distance = distance_values(closest_index, :);

% Display information
fprintf("\nClosest distance achievable changing only L and k values while ");
fprintf("also maintaining at least 9 bounces\nand remaining close to the advertised ");
fprintf("2g acceleration: %.2fm at %.1fs, with max %.2fg acceleration.\n",...
    closest_distance(2), closest_distance(1),...
    closest_distance(4));

fprintf("L value: %.2f\n", closest_distance(5));
fprintf("k value: %.2f\n", closest_distance(6));

% Across these questions, the results of these simulations will yield 
% different values if the initial model is modified to exclude the assumptions made. 
%
% In section 5.6, considering the precision to create a true 'water touch' 
% experience, this simulation will require extensive research, and in turn 
% its application will require significant support in terms of variation in 
% the controllable parameters.

%% 6 Conclusion
%
% This report answers the proposals queries with sufficient results to
% further develop this component of the 'New World City' transformation.
% It is intented to showcase the viability of the proposal, and is
% limited by the assumptions made. There are many more models to be 
% generated before begnning production on such a project.
% 
% A simulation must be configured that neccessitates fewer assumptions, 
% such as testing across a range of weights and tidal heights. It must also 
% discuss the viability in addressing these, such as material limitations, 
% cost effectiveness, and safety requirements of utilising cords of 
% various lengths and spring constants, which will directly vary in 
% response to a change in mass in order to sustain a uniform experience,
% where each of the key requirements within the proposal are sufficiently
% met.