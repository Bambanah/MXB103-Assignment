function I = simpsonsrule(f,a,b,n)
% simpsonsrule(f, a, b, b) returns a Simpson's Rule approximation for the
% integral of f from x = a to x = b using n subintervals where f is an
% array of the function values and n must be even.
% I returns the area under the graph of the function f.

% Compute sub-interval width
h = (b-a)/n;

% Initialise sum counters
sum_even=0;
sum_odd=0;

% Sum all even numbers
for i=2:2:n
    sum_even = sum_even + f(i);
end

% Sum all odd numbers
for i=3:2:n
    sum_odd= sum_odd + f(i);
end

% Calculate integral
I = (h/3)*(f(1)+ f(n+1) + 4*(sum_even)+ 2*(sum_odd));
end