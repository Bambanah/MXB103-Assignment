function [x,i] = secant(f, x0, x1, tol, maxiters)
%SECANT Secant method
% [X,I] = SECANT(F, X0, X1, TOL, MAXITERS) performs the secant
% method with F(x), starting at x_0 = X0 and x_1 = X1, and continuing
% until either |X_i+1 - X_i| <= TOL, or MAXITERS iterations have
% been taken. The number of iterations, I, is also returned.
% An error is raised if the first input is not a function handle.
% A warning is raised if the maximum number of iterations is reached
% without achieving the tolerance.

if ~isa(f, 'function_handle')
    error('Your first input was not a function handle')
end

i = 0;          % initialise our counter
xa = x0;
xb = x1;
x = x1; 
x_old = inf;    % we dont have a value of x-old to begin
%fprintf('%i %s %1.9f %1.9f %1.9f\n', i, '     ', xa, xb, x_old)
while abs(x - x_old)> tol && i < maxiters
     x_old = x;
    x = xb - f(xb)*( xb - xa)/( f(xb) - f(xa) );    % compute the new x
    i = i + 1;          % increase our counter by one
    %update the values
    xa =xb;
    xb = x;
    %fprintf('%i %s %1.9f %1.9f %1.9f\n', i, '     ', xa, xb, x)
 
end
if abs(x - x_old) > tol
    warning('Maximum number of iterations reached without achieving tolerance.')  
end


