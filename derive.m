function derivative = derive(t,v)
% derive(T,V) calculates the derivative of the function given by the 
% data points (t, v)
% The derivative at the first points is calculated using a forward difference
% The derivative at the end points is calculated using a backward difference
% The derivative at all other interior points is calculated using the central
% difference formula
% Input: vectors for the data points for time and velocity
% Output: The vector derivative returns the values of the derivative at each 
% of the data points of t.

n = length(t);
derivative = zeros(1,n);
derivative(1) = ( v(2) - v(1))/(t(2) - t(1));  % forward difference

for j = 2:n-1
    derivative(j) = ( v(j+1) - v(j-1))/(t(j+1) - t(j-1));  % central difference
end
derivative(n) = ( v(n) - v(n-1))/(t(n) - t(n-1)); % backward difference

end
